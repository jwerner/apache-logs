

<?php
require 'vendor/autoload.php';

$logfile = null;
$logfile = '\\\\deruewdce308w4l.ad.opel.com\\c$\\xampp\\apache\\logs\\access_2019-12-12__W49.log';
$logfile    = 'access.log';

// Set default parameters

// Check your log format from your Apache config:
$logformat  = "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"";
// Base dir for all log files on this server:
$logdir     = '/var/log/httpd';
$pathSep    = '/';

$mode               = 'help';
$select_mode        = 'display';
$csvSeparator       = ';';
$maxLines           = 10;
$allLines           = false;
$showLineNumbers    = false;
$filters            = array();

// Do we have a local config file?
if(file_exists(__DIR__.'/config.php')) {
    // Read file, override defaults:
    $config = require __DIR__.'/config.php';
    extract($config);
}

// {{{ Check an parse parameters

// Check logdir:
if(!is_dir($logdir))
    die('ERROR! Configuration for logdir is invalid.');

// Parse mode:
if(!empty($_GET['mode']) and in_array($_GET['mode'], array('help', 'select', 'display', 'csv')))
    $mode = $_GET['mode'];

// Parse logfile:
if(!empty($_GET['logfile']) and is_file($logdir.$pathSep.$_GET['logfile']))
    $logfile = $logdir.$pathSep.$_GET['logfile'];

// Parse csvSeparator
if(isset($_GET['c;svSeparator']) and in_array($_GET['csvSeparator'], array(',', ';', '|', 'TAB'))) {
    $csvSeparator = $_GET['csvSeparator'];
}
if($csvSeparator==='TAB')
    $csvSeparator = "\t";

// Parse maxLines:
if(isset($_GET['maxLines']) and (preg_match('/^\d+$/', $_GET['maxLines'], $matches) or $_GET['maxLines']==='0')) {
    $maxLines = (int)$_GET['maxLines'];
}

// Parse allLines:
if(isset($_GET['allLines']) and $_GET['allLines']==='true') {
    $allLines = true;
}
if($allLines===true)
    $maxLines = 0;

// Pares showLineNumbers:
if(!empty($_GET['showLineNumbers']) and $_GET['showLineNumbers']==='true')
    $showLineNumbers = true;

// Parse filters
$startEndRegex='/';
if(!empty($_GET['filters']) and is_array($_GET['filters'])) {
    foreach($_GET['filters'] as $fKey=>$fVal) {
        if(in_array($fKey, array('host', 'logname', 'user', 'stamp', 'time', 'request', 'status', 'responseBytes', 'HeaderReferer', 'HeaderUserAgent'))) {
            if(substr($fVal, 0, 6)==='regex:') {
                $fVal = substr($fVal, 6);
                if(substr($fVal, 0, 1)!==$startEndRegex)
                    $fVal = $startEndRegex.$fVal;
                if(substr($fVal, -1)!==$startEndRegex)
                    $fVal = $fVal . $startEndRegex;
                $filters[$fKey] = 'regex:'.urldecode($fVal);
            } else {
                $filters[$fKey] = $fVal;
            }
        }
    }
}
// }}} end checks

ob_start();

printf( '<h2>File: %s, Mode: %s, maxLines: %s</h2>', basename($logfile), $mode, $maxLines==0 ? '(all)' : $maxLines);
if($filters!==array()) {
    echo '<h2>Filters</h2>'; 
    echo '<table>';
    foreach($filters as $fKey=>$fVal)
    {
        echo '<tr>';
        echo '<th>'.$fKey.':</th><td>'.$fVal.'</td>';
        echo '</tr>';
    } 
    echo '</table>';
} else {
    echo '<p>No filters selected.</p>'; 
}

if(in_array($mode, array('csv', 'display')) and !file_exists($logfile)) {
    $mode = 'error';
    echo '<p>ERROR! Logfile '.basename($logfile).' doenot exist.</p>';
}


// Simply instantiate the class :
$parser = new \Kassner\LogParser\LogParser();
$parser->setFormat($logformat);

// And then parse the lines of your access log file :



switch($mode)
{
    case 'help': // {{{ 
        echo '<pre>';
        echo <<< EOL
Logfiles analyzer

Allowed GET parameters:

mode:               Either select, csv or display.
                    Defaults to help.

maxLines:           Either 0 or a number greater than 0.
                    Number of first lines to show
                    Defaults to 10.

allLines:           if true, all lines will be shown.

showLineNumbers:    if true, line numbers will be shown.

csvSeparator:       For mode=csv. How are columns separated?
                    Allowed values: ; , | TAB 
                    Defaults to ;
        
logfile:            Filename without path of logfile to be analyzed

filters:            Array of key=>value pairs to filter columns for.
                    Allowed keys: host; logname; user; stamp; time; request; status; responseBytes; HeaderReferer; HeaderUserAgent
                    E.g. filters[user]=abcde
                    To use a regular expression, prepend the regex value with regex:,
                    e.g. filters[request]=regex:GET \/index.php

EOL;
        echo '</pre>';    
        echo '<hr><p><a href="index.php?mode=select">Select a logfile</a></p>';
        $content  = ob_get_clean();
        parseTemplate('layout', array('content'=>$content));
        break; // }}}
    case 'error': // {{{ 
        echo '<p><a href="index.php?mode=select">Select another file</a></p><hr>';
        $content  = '<br><br><br>'.ob_get_clean().'<br><br><br>';
        parseTemplate('layout', array('content'=>$content));
        break; // }}} 
    case 'select': // {{{ 
        $files = glob($logdir.'\\access*.log');
        usort($files, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));
        echo '<h2>Select a Log File</h2>';
        echo '<p><a href="#bottom">Go to bottom</a></p><hr>';
        echo '<table borders="0">';
        echo '<tr><th>File</th><th>Size</th><th>Changed</th></tr>';
        foreach($files as $file) {
            echo '<tr>';
            printf('<td><a href="index.php?mode=%s&showLineNumbers=true&logfile=%s&allLines=true">%s</a></td><td style="text-align:right;padding-right:7px">%s bytes</td><td>%s</td>', $select_mode, urlencode(basename($file)), basename($file), number_format(filesize($file), 0, '.', ',' ), date('Y-m-d H:i:s', filemtime($file)) );
            echo '</tr>';
        }
        echo '</table>';
        $content = ob_get_clean();
        parseTemplate('layout', array('content'=>$content));
        break; // }}}
    case 'display': // {{{ 
       // }}}  
    case 'csv': // {{{ 
        $lines = file($logfile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        echo '<p><a href="index.php?mode=select">Select another file</a></p><hr>';
        if($mode=='csv')
            echo '<pre>';
        if($mode=='display') {
            echo "\r\n\r\n".'<table class="display">'."\r\n";;
            $csvSeparator = '</td><td>';
        }
        $i=0;
        $countProps = 0;
        foreach ($lines as $line)
        {
            $printedLine = '';
            if($mode=='display')
                $printedLine .= "\r\n<tr><td>";
            $excludeLine = false;
            if($i==$maxLines and $i>0)
                break;
            try {
                $entry = $parser->parse($line);
                // Print header row before first line?
                if($i==0) { // {{{ 
                    $countProps = count((array)$entry);
                    if($showLineNumbers === true) {
                        $printedLine .= 'linenumber'.$csvSeparator;
                    }
                    $iColumn = 0;
                    foreach($entry as $key=>$value) {
                        $printedLine .=  $key;
                        if($iColumn<$countProps-1)
                            $printedLine .=  $csvSeparator;
                        $iColumn++;
                    }
                    if($mode=='csv')
                        $printedLine .=  "\r\n";
                    if($mode=='display')
                        $printedLine .=  "</td></tr>\r\n";
                    echo $printedLine;
                    $printedLine ='';
                    if($mode=='display')
                        $printedLine = "\r\n<tr><td>";
                } // }}}  
                // Continue with data lines
                $iColumn = 0;
                if($showLineNumbers === true) {

                    $printedLine .= ($i+1).$csvSeparator;
                }
                // Loop through all properties
                foreach($entry as $key=>$value)
                {
                    if(isset($filters[$key])) {
                        $fKey = $filters[$key];
                        // DBG $printedLine .= 'filter for '.$key.'|';
                        if(substr($fKey, 0, 6)==='regex:') {
                            // DBG $printedLine .=  'regex:'.$fKey.'|';
                            $fKey = substr($fKey, 6);
                            if(preg_match($fKey, $value, $matches)) {
                            } else {
                                $excludeLine = true;
                            }
                        } else {
                            if($value!==$filters[$key])
                                $excludeLine = $excludeLine || true;
                        }
                        if($excludeLine===true) {
                            // DBG $printedLine .=  'exclude>';
                        } else {
                            // DBG $printedLine .=  'include>';
                        }
                    }
                    if(!in_array($key, array('stamp', 'status', 'responseBytes')))
                        $printedLine .=  '"'.$value.'"';
                    else
                        $printedLine .=  $value;
                    if($iColumn<$countProps-1)
                        $printedLine .=  $csvSeparator;
                    $iColumn++;
                }
                if($excludeLine) {
                    // DBG $printedLine = 'EXCLUDE>>'.$printedLine;
                }
            } catch(Exception $e) {
                if($showLineNumbers === true) {
                    $printedLine .= ($i+1).$csvSeparator;
                    $printedLine .= $e->getMessage();
                }
            }
            if($mode=='csv')
                $printedLine .= "\r\n";
            if($mode=='disply')
                $printedLine .= '</td></tr>';
            if($excludeLine!==true) {
                echo $printedLine;
            } else {
            }

            $i++;
        }
        if($i===0)
            echo 'File does not contain any line.'."\r\n";
        if($mode=='csv')
            echo '</pre>';
        if($mode=='display')
            echo '</table>';
        $content  = ob_get_clean();

        parseTemplate('layout', array('content'=>$content));
        break; // }}} 
    default: // {{{ 
        $content = ob_get_clean();
        $content .= '<br><br><hr>'.'ERROR! Mode '.$mode.' is not supported.'.'<hr>';
        parseTemplate('layout', array('content'=>$content));
        break; // }}} 
}

function parseTemplate($tmpl, $vars = array(),  $echo=true)
{
    $file = 'templates/'.$tmpl.'.php';
    if(!is_file($file))
        die('ERROR! Template '.$file. ' doesn\'t exist.');

    extract($vars);
    ob_start();
    include($file);
    $parsed = ob_get_clean();

    if($echo===true) {
        echo $parsed;
        return;
    }

    return $parsed;
}
?>

