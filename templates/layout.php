<!DOCTYPE html>
<html lang="en">
<head>
<title>Kassner/LogParser</title>
<style type="text/css">
<!--
table.display td {
    border-collapse: collapse;
    border: 1px solid #eee;
}
table.display td {
    vertical-align:top;
}
-->
</style>
</head>
<body>
<h1>Kassner/LogParser</h1>
<?= $content ?>
<hr>
Based on <a href="https://github.com/kassner/log-parser">kassner/log-parser</a> | <a href="<?=$_SERVER['PHP_SELF'] ?>?mode=help">Help</a>
<a name="bottom"></a>
</body>
</html>
