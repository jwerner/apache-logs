% Apache Logs Parser/Filter
% 2019-12-19
% Joachim Werner <joachim.werner@diggin-data.de> 

# About

This script uses [kassner/log-parse](https://github.com/kassner/log-parser) to show or filter Apache logfiles.

Logfiles can be displayed as HTML tables or as CSV text.

Logfiles can be filtered by their columns.

# Installation:

* Clone this repository into a directory where your PHP enabled web server can show it
* Open a shell/cmd in the cloded directory
* Install composer dependencies: `composer install`

# Configuration

See [index.php?mode=help](index.php?mode=help)

You can set some default parameters by copying config.php.sample to config.php and editing it.

# Usage

See [index.php?mode=help](index.php?mode=help)


